# spiral_matrix

Check if a given matrix is square and then print it in spiral form i.e 1 2 3 4 5 6 7 8 9 is a square matrix, so the matrix would be <br>
[1 2 3 <br>
4 5 6 <br>
7 8 9 ], and should be printed as a result the following 1 2 3 6 9 8 7 4 5.