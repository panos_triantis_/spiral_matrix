from math import sqrt
from copy import deepcopy

def spiral_matrix(num_list):
	check = sqrt(len(num_list))
	if check != int(check):
		print('\nInput: {}\n'.format(num_list))
		return '\nNot a valid square matrix\n'
	num_list_cp = deepcopy(num_list)
	num_list2 = []
	lst = [i for i in range(int(check), len(num_list)+1, int(check))]
	for i in range(int(sqrt(len(num_list))),0,-1):
		num_list2 += num_list[:i]
		del num_list[:i]
		num_list2 += (num_list[::-i])[::-1]
		del num_list[::-i]
		num_list = num_list[::-1]

	print('Input Matrix:')
	for i,z in enumerate(lst):
		lower_boundary = i
		if i > 0:
			lower_boundary = z-int(check)
		upper_boundary = z
		print(num_list_cp[lower_boundary:upper_boundary])
	
	# print('Output: {}'.format(num_list2))

	return num_list2


print('Matrix spiral form:\n', spiral_matrix([i for i in range(1,10)]))

print('Matrix spiral form:\n', spiral_matrix([i for i in range(1,13)]))

print('Matrix spiral form:\n', spiral_matrix([i for i in range(1,17)]))

print('Matrix spiral form:\n', spiral_matrix([i for i in range(1,26)]))